$vowels = "aeiou".split("")

def is_vowel?(char)
  return $vowels.include?(char)
end

def vowel_place(word)
  chars = word.split("")
  i = 0

  while i < chars.length
    return i + 1 if is_vowel?(chars[i]) && chars[i] == "u"
    return i if is_vowel?(chars[i])
    i += 1
  end
end


def translate(string)

# need to account for "u"


  split_string = string.split(" ")
  pig_string = []
  i = 0

  while i < split_string.length
    word = split_string[i]

    if is_vowel?(word[0])
      pig_string << word + "ay"
    else
      pig_string << word[vowel_place(word)..-1] +
      word[0..vowel_place(word) - 1] + "ay"
    end
    i += 1
  end
  return pig_string.join(" ")
end

def add(num1, num2)
  return num1 + num2
end

def subtract(num1, num2)
  return num1 - num2
end

def sum(array)
  return array.inject(0, :+)
end

def multiply(*num)
  product = 1

  num.each do |number|
    product *= number
  end
  return product
end

def power(num1, num2)
  return num1 ** num2
end

def factorial(num)
  answer = 1
  for i in 2..num
    answer *= i
  end
  return answer
end

def echo(str)
  return "#{str}"
end


def shout(str)
  return "#{str}".upcase
end


def repeat(str, num_times = 2)
  return ("#{str} " * num_times).strip
end


def start_of_word(str, index)
  i = 0
  first_letters = ""
  while i < index
    first_letters += str[i]
  i += 1
  end
  return first_letters
end


def first_word(string)
  string.split(" ")
  string.split[0]
end

def titleize(string)
  split_string = string.split(" ")
  new_string = []
  i = 0
  split_string.each do |word|
    if (word.length <= 4 && (word.equal?(split_string[0]) || word.equal?(split_string[-1])))
      new_string << word.capitalize
    elsif (word.length <= 4)
      new_string << word
    else
      new_string << word.capitalize
    end
  end
  return new_string.join(" ")
end
